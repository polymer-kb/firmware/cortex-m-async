#![no_std]
#![feature(never_type, const_fn)]

extern crate alloc;

/// VecDeque-backed async MPMC Queue
pub mod mpmc;

/// "Atomic" "woken" flag
pub mod flag;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
